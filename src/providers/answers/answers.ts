import { Injectable } from '@angular/core';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Answer } from '../../models/answer';
import { Question } from '../../models/question';
import { Questions } from '../../providers/questions/questions';

/*
  Generated class for the AnswersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
@Component ({
    providers: [Questions]
})
export class Answers {
    answers: Answer[] = [];

    public _Url = 'http://localhost:8000/api/';

  constructor(private http: HttpClient) {
  }

  add(answer: Answer, question: Question) {
    let newAnswer = new Answer(this.getNextId(), answer.title, question.id, 0, true);

    this.http.post(this._Url + 'answer/create', newAnswer)
      .subscribe(
        answer => Questions[question.id].answers.push(newAnswer)
      );
  }

  edit(answer: Answer) {
    console.log(answer);
  }

  delete(answer: Answer) {
    this.answers.splice(this.answers.indexOf(answer), 1);
  }

  getAll() {
    return this.answers;
  }

  getNextId() {
    return (this.answers.slice(-1)[0].id + 1);
  }

  editStatus(question, answer) {
    let answerId = answer.id;
    let newStatus: boolean = (answer.status == 1 ? false : true);
    let updatedAnswer = new Answer((answerId + 1), answer.title, question.id, answer.votes, newStatus);

    this.http.put(this._Url + 'answer/' + answerId + '/update', updatedAnswer)
      .subscribe(answer => {
      });

    return updatedAnswer;
  }

  addVote(answer) {

    this.http.post(this._Url + 'answer/' + answer.id + '/vote', answer)
      .subscribe(
        answer => console.log(answer),
        error => console.log('deu erro')
      );


      return answer;
  }

}
