import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { Question } from '../../models/question';
import { Answer } from '../../models/answer';


/*
  Generated class for the QuestionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Questions {
  questions: any = [];
  questionsVote: any = [];

  public _Url = 'http://localhost:8000/api/';

  constructor(private http: HttpClient) {
    this.http.get(this._Url + 'questions')
      .subscribe(
        questions => this.questions = questions,
        error => 'Deu erro!!'
      );

  }

  add(question: Question) {
    let newQuestion = new Question(this.getNextId(), question.title);

    this.http.post(this._Url + 'question/create', newQuestion)
      .subscribe(question => {
        question["answers"] = [];
        this.questions.push(question);
      });
  }

  delete(question: Question) {
    this.questions.splice(this.questions.indexOf(question), 1);
  }

  editTitle(question: Question) {

      this.http.put(this._Url + 'question/' + question.id + '/update', question)
      .subscribe(ret => {
        this.questions[question.id -1].title = question.title;
      });

      return this.questions[question.id];
  }

  getAll() {

    this.http.get(this._Url + 'questions')
      .subscribe(
        questions => this.questions = questions,
        error => 'Deu erro!!'
      );

    return this.questions;
  }

  getAllNotVotate() {
    let questionsNotVoted: any = [];
    let questionsVote: number[] = this.questionsVote;

    if (questionsVote) {

      this.questions.map(function(q) {

          if (questionsVote.indexOf(q.id) > -1) {
            return;
          }
          questionsNotVoted.push(q);
      });

      return questionsNotVoted;
    }

    return this.questions;

  }

  getById(questionId) {
    return this.questions[questionId];
  }


  getNextId() {
    if (this.questions.length !== undefined && this.questions.length < 1) {
      return 1;
    }

    return (this.questions.slice(-1)[0].id + 1) ;
  }

  getAnswers(question: Question) {
    if (question) {
      return this.questions[question.id].answers;
    }
  }

  addAnswer(answer: Answer, question: Question) {
    let newAnswer = new Answer(null, answer.title, question.id, 0, true);

    this.http.post(this._Url + 'answer/create', newAnswer)
      .subscribe(answer => {
        this.questions[question.id-1].answers.push(answer);
      });
  }

  editQuestionAnswerStatus(question, answerUpdated) {

    this.questions[question.id -1].answers.map(function(a) {
      if (a.id == answerUpdated.id -1) {
        a.status = answerUpdated.status;
      }
    });

    return this.questions[question.id -1];
  }

}
