import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Question } from '../../models/question';
import { Questions } from '../../providers/questions/questions';

/**
 * Generated class for the QuestionDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question-detail',
  templateUrl: 'question-detail.html',
})
export class QuestionDetailPage {
    question: Question[];

    constructor(public navCtrl: NavController, public navParams: NavParams, questions: Questions) {
        this.question = navParams.get('question');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad QuestionDetailPage');
    }

    editQuestion(question: Question) {
        this.navCtrl.push('QuestionEditPage', {
            question: question
        });
    }

}
