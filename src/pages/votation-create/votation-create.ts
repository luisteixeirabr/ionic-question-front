import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { Questions } from '../../providers/questions/questions';

/**
 * Generated class for the VotationCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-votation-create',
  templateUrl: 'votation-create.html',
})
export class VotationCreatePage {

  currentQuestions: any = [];

  constructor(public navCtrl: NavController, public questions: Questions, public navParams: NavParams, public modalCtrl: ModalController) {
    this.currentQuestions = this.questions.getAll();

  }

  ionViewDidLoad() {
  }

  addQuestion() {
    let addModal = this.modalCtrl.create('QuestionCreatePage');
    addModal.onDidDismiss(question => {
      if (question) {
        this.questions.add(question);
      }
    })
    addModal.present();
  }

  /**
   * Edit a question title from the list of questions.
   */
  editTitle(question) {
    let addModal = this.modalCtrl.create('QuestionEditPage', {question: question});
    addModal.onDidDismiss(question => {
      if (question) {
        this.questions.editTitle(question);
      }
    })
    addModal.present();
  }

  /**
   * List all answers from a question.
   */
  listAnswers(question) {
    this.navCtrl.push('AnswersListPage', {question: question});
  }

  editAnswers(question) {
    let addModal = this.modalCtrl.create('AnswersListPage', {question: question});
    addModal.onDidDismiss(answer => {
      if (answer) {
       // this.answers.edit(answer);
      }
     })
    addModal.present();
  }

  /**
   * Return a color by answer status
   */
  getAnswerStatus(status) {
    if (status==0) {
      return 'light';
    }
  }

}
