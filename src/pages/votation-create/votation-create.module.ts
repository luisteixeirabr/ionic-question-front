import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VotationCreatePage } from './votation-create';

@NgModule({
  declarations: [
    VotationCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(VotationCreatePage),
  ],
})
export class VotationCreatePageModule {}
