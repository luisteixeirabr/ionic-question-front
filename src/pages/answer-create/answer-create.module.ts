import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnswerCreatePage } from './answer-create';

@NgModule({
  declarations: [
    AnswerCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(AnswerCreatePage),
  ],
})
export class AnswerCreatePageModule {}
