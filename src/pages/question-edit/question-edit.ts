import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Question } from '../../models/question';
import { Questions } from '../../providers/questions/questions';

/**
 * Generated class for the QuestionEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question-edit',
  templateUrl: 'question-edit.html',
})
export class QuestionEditPage {

    public question: Question;

    form: FormGroup;

    isReadyToSave: boolean;

  constructor(public navCtrl: NavController, public questions: Questions, public viewCtrl: ViewController, public navParams: NavParams, formBuilder: FormBuilder, public modalCtrl: ModalController) {
    this.question = this.navParams.data.question || {};

    this.form = formBuilder.group({
        id: [this.question.id],
        title: [this.question.title, Validators.required]
    })

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    })
  }

    ionViewDidLoad() {
    }

    editAnswer() {

    }

    saveQuestion() {
    }

    /**
     * The user cancelled, so we dismiss without sending data back.
     */
     cancel() {
        this.viewCtrl.dismiss();
     }

    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
     done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

}
