import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionEditPage } from './question-edit';

@NgModule({
  declarations: [
    QuestionEditPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionEditPage),
  ],
})
export class QuestionEditPageModule {}
