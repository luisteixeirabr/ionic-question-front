import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { Questions } from '../../providers/questions/questions';
import { Answers } from '../../providers/answers/answers';

/**
 * Generated class for the AnswersListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-answers-list',
  templateUrl: 'answers-list.html',
})
export class AnswersListPage {
  question: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public questions: Questions, public answers: Answers, public modalCtrl: ModalController) {
    this.question = this.navParams.data.question || [];
  }

  ionViewDidLoad() {
  }

  addAnswer(question) {
    let addModal = this.modalCtrl.create('AnswerCreatePage', {question_id: question.id});

    addModal.onDidDismiss(answer => {
      if (answer && question) {
        this.questions.addAnswer(answer, question);
      }
    })

    addModal.present();
  }

  editStatus(question, answer) {
    let answerUpdate = this.answers.editStatus(question, answer);
    this.question = this.questions.editQuestionAnswerStatus(question, answerUpdate);
  }

  getAnswerStatus(status) {
    if (status == 1) {
      return true;
    }
    return false;
  }

  // editAnswer(answer) {
  // }

  // removeAnswer(event) {

  // }

  // ngOnChanges(changes: {[propertyName: string]: SimpleChange}){
  //   for (let propName in changes) {
  //       console.log(changes[propName]);

  //   }
  // }
}
