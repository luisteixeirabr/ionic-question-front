import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnswersListPage } from './answers-list';

@NgModule({
  declarations: [
    AnswersListPage,
  ],
  imports: [
    IonicPageModule.forChild(AnswersListPage),
  ],
})
export class AnswersListPageModule {}
