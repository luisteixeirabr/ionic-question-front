import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionVotePage } from './question-vote';

@NgModule({
  declarations: [
    QuestionVotePage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionVotePage),
  ],
})
export class QuestionVotePageModule {}
