import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Questions } from '../../providers/questions/questions';
import { Answers } from '../../providers/answers/answers';

/**
 * Generated class for the QuestionVotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question-vote',
  templateUrl: 'question-vote.html',
})
export class QuestionVotePage {
  questions: any = [];
  provider: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public questionsProvider: Questions, public answers: Answers) {
      this.provider = questionsProvider;
      this.questions = questionsProvider.getAllNotVotate();
  }

  ionViewDidLoad() {
  }

  vote(answer): void {
      this.answers.addVote(answer);
      this.provider.questionsVote.push(parseInt(answer.question_id));
      this.questions = this.provider.getAllNotVotate();
  }

}
