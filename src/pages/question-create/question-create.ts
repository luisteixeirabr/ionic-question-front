import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the QuestionCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question-create',
  templateUrl: 'question-create.html',
})
export class QuestionCreatePage {
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, formBuilder: FormBuilder) {

        this.form = formBuilder.group({
          title: ['', Validators.required]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
          this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, so we dismiss without sending data back.
     */
     cancel() {
        this.viewCtrl.dismiss();
     }

    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
     done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

}
