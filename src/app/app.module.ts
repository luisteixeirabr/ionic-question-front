import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { VotationCreatePage } from '../pages/votation-create/votation-create';
import { QuestionVotePage } from '../pages/question-vote/question-vote';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Questions } from '../providers/questions/questions';
import { Answers } from '../providers/answers/answers';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    VotationCreatePage,
    QuestionVotePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    QuestionVotePage,
    VotationCreatePage
  ],
  providers: [
    Questions,
    Answers,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
