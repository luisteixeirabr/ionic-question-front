export class Answer {
    constructor(
    public id: number,
    public title: string,
    public question_id: number,
    public votes: number,
    public status: boolean) {}
}
