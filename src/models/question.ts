import { Answer } from './answer';

export class Question {
    constructor(
      public id: number,
      public title: string,
      public answers?: Answer[]){}
}
